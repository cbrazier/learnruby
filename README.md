# README

This is a test project for my own self development and learning with Ruby on Rails.

It is an adaption of the tasks set out from the trial day for the Today I Have project.

The Ruby version is 2.6.3 and the Rails version is 6.0.0
