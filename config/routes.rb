Rails.application.routes.draw do

  root 'pages#index'
  get 'about', to: 'pages#about'

  resources :posts
  resources :users, except: [:new]
  devise_for :users, path:'auth', path_names: { sign_in: 'login',
                                                sign_out: 'logout', sign_up: 'register'}

end
