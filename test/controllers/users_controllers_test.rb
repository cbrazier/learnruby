require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = users(:test_user_one)
  end

  test "should get user index" do
    sign_in @user
    get users_path
    assert_response :success
    assert_select 'h1', 'All Users'
  end

  test "should get new" do
    get new_user_registration_path
    assert_response :success
  end

  test "should show user" do
    sign_in @user
    get user_url(@user)
    assert_response :success
  end

  test "should be redirected when unauthorised" do
    get users_path
    assert_response :redirect
    get user_path(@user)
    assert_response :redirect
    get edit_user_path
    assert_response :redirect
    patch user_path(@user)
    assert_response :redirect
  end

end
