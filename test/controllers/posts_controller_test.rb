require 'test_helper'
g
class PostsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @post = posts(:test_post_one)
    ####
  end

  test "redirect responses when not authorised" do
    get new_post_url
    assert_response :redirect

    get edit_post_url(@post)
    assert_response :redirect

    post posts_path(@post)
    assert_response :unauthorized

    delete post_path(@post)
    assert_response :redirect

    patch post_url(@post)
    assert_response :redirect

  end

  test "should get index" do
    get posts_url
    assert_response :success
    assert_select 'h1', 'All Posts'
    assert_select 'a', 'This is the first test title!'
  end

  test "should get new" do
    sign_in users(:test_user_one)
    get new_post_url
    assert_response :success
  end

  test "should create post" do
    sign_in users(:test_user_one)
    assert_difference('Post.count') do
      post posts_url, params:{
          post: {
              title: @post.title,
              description: @post.description
          }
      }
    end
    assert_redirected_to post_url(Post.last)
  end

  test "should show post" do
    get post_url(@post)
    assert_response :success
  end

  test "should get edit" do
    sign_in users(:test_user_one)
    get edit_post_url(@post)
    assert_response :success
  end

  test "should update post" do
    sign_in users(:test_user_one)
    patch post_url(@post), params: {
        post: {
            title: @post.title,
            description: @post.description
        }
    }
    assert_redirected_to post_url(@post)
  end

  test "should destroy post" do
    sign_in users(:test_user_one)
    assert_difference('Post.count', -1) do
      delete post_url(@post)
    end
    assert_redirected_to posts_url
  end

end