require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers

  test "create an account" do
    visit new_user_registration_path
    assert_text "Signup"

    fill_in "Username", with: "Jables"
    fill_in "Email", with: "tonyhawk@example.com"
    fill_in "Full name", with: "ja boy steve"
    fill_in "Password", with: "password"
    click_button "Sign up"

    assert_text "Welcome to Today I Have Jables"

  end

end
