require "application_system_test_case"

class PostsTest < ApplicationSystemTestCase

  setup do
    @post = posts(:test_post_one)
  end

  test "visiting the posts index" do
    visit posts_url
    assert_selector "h1", text: "All Posts"
  end

  test "creating a post" do
    visit posts_url
    click_on "Create Post"

    assert_text "Create a new Post"
    fill_in "Title", with: @post.title
    fill_in "Description", with: @post.description
    click_button "Create Post"

    assert_text "Post was successfully created!"
    assert_text @post.title
    assert_text @post.description
  end

  test "editing a post" do
    visit posts_url
    click_on "Edit", match: :first

    assert_text "Edit Post"
    fill_in "Title", with: "This is an edited test post"
    fill_in "Description", with: "This is an edited test post description"
    click_on "Update Post"

    assert_text "Post was updated!"
    assert_text "This is an edited test post"
    assert_text "This is an edited test post description"
  end

  test "destroy a post" do
    visit posts_url
    page.accept_confirm do
      click_on "Delete", match: :first
    end
    assert_text "Post was deleted"
  end

end