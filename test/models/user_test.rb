require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "user fields must not be empty" do
    user = User.new
    assert user.invalid?
    assert user.errors[:username].any?
    assert user.errors[:email].any?
    assert user.errors[:full_name].any?
    assert user.errors[:encrypted_password].any?
  end

  test "user fields are all populated" do
    user = User.new(email:  "example@example.com", username: "bigc")
    assert user.invalid?
    user = users(:test_user_one)
    assert user.valid?
  end

  test "username field is unique" do
    user =  users(:test_user_one)
    assert user.valid?
    user.save
    user = User.new(username: "elpolloloco", email: "jib@gmail.com",
                    full_name: "john mayer", encrypted_password: "poopoopo" )
    assert user.invalid?
  end

  test "email field is unique" do
    user =  users(:test_user_one)
    assert user.valid?
    user.save
    user = User.new(username: "bigjohn", email: user.email,
                    full_name: "john mayer", encrypted_password: "poopoopo" )
    assert user.invalid?
  end

  test "user email is not valid" do
    user = users(:bad_email)
    assert user.invalid?
  end

  test "user fields too short" do
    user = User.new(username: "elpolloloco", email: "jib@gmail.com",
                    full_name: "jo", encrypted_password: "poopoopo" )
    assert user.invalid?
    user = User.new(username: "el", email: "jib@gmail.com",
                    full_name: "john mayer", encrypted_password: "poopoopo" )
    assert user.invalid?
    user = User.new(username: "elpolloloco", email: "jib@gmail.com",
                    full_name: "john mayer", encrypted_password: "p" )
    assert user.invalid?
  end

end
