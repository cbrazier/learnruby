require 'test_helper'

class PostTest < ActiveSupport::TestCase

 test "post fields must not be empty" do
    post = Post.new
    assert post.invalid?
    assert post.errors[:title].any?
    assert post.errors[:description].any?
    assert post.errors[:user_id].any?
  end

  test "post fields must not be too short" do
    post = Post.new(title: "I", description: "yep")
    assert post.invalid?
    post = Post.new(title: "Relevant title", description: "This description is big",
                    user_id: users(:test_user_one).id)
    assert post.valid?
    post = posts(:test_post_one)
    assert post.valid?
  end

end
