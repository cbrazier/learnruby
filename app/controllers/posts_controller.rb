class PostsController < ApplicationController

  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    @posts = Post.paginate(page: params[:page], per_page: 5).search(params[:term])
  end

  def new
    @post = Post.new
    @post.tag.build
  end

  def show
  end

  def create
    #render plain: params[:post].inspect
    @post = Post.new(post_params)
    @post.user = current_user

    if @post.save
      flash[:success] = "Post was successfully created!"
      redirect_to post_path(@post)
    else
      render 'new'
    end

  end

  def edit
    if @post.tag.count == 0
      @post.tag.build

    end
  end

  def update
    update_params = remove_tags
    if @post.update(update_params)
      flash[:success] = "Post was updated!"
      redirect_to post_path(@post)
    else
      flash[:success] = "Post was not updated"
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "Post was deleted"
    redirect_to posts_path
  end

  private

    def post_params
      params.require(:post).permit(:title, :description, :tag_names, :term)
    end

    def set_post
      @post = Post.find(params[:id])
    end

    def remove_tags
      arr = params.require(:post).permit(:title, :description, :tag_names)
      new_tags = arr[:tag_names].split(',')
      old_tags = @post.tag.map(&:tag_name)

      del_tags = old_tags - new_tags
      update_tags = (new_tags - old_tags).join(',')

      del_tags.each do |tag|
        Tag.find_by_tag_name(tag).destroy
      end

      arr[:tag_names] = update_tags
      arr

    end

end