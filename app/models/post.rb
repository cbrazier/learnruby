class Post < ActiveRecord::Base

  belongs_to :user
  has_many :tag, dependent: :destroy
  accepts_nested_attributes_for :tag

  validates :user_id, presence: true
  validates :title, presence: true, length: { minimum: 3, maximum:50 }
  validates :description, presence: true, length: { minimum: 10, maximum: 300 }

  def tag_names
    tag.map(&:tag_name).join(',')
  end

  def tag_names=(tags)
    self.tag_attributes = tags.split(',').map do |tag|
      {tag_name: tag}
    end
  end

  def self.search(term)
    if term
      joins(:user, :tag).where('title LIKE ? or description Like ? or users.username LIKE ? or tags.tag_name LIKE ?',
            "%#{term}%", "%#{term}%", "%#{term}%", "%#{term}%").order('created_at DESC')
    else
      order('created_at DESC')
    end
  end


end