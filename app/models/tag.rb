class Tag < ActiveRecord::Base

  belongs_to :post

  validates :tag_name, presence: true


end
