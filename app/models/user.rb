class User < ActiveRecord::Base

  has_many :posts
  devise :database_authenticatable, :registerable

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :username,
            presence: true,
            uniqueness: { case_sensitive: false },
            length: { minimum: 3, maximum: 25 }
  validates :email,
            presence: true,
            uniqueness: { case_sensitive: false},
            length: { maximum: 105 },
            format: { with: VALID_EMAIL_REGEX }
            before_save { self.email = email.downcase}
  validates :full_name,
            presence: true,
            length: { minimum: 3, maximum: 30 }
  validates :encrypted_password,
            presence: true

end
